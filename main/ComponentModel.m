classdef ComponentModel < PhysicalModel
    
    properties
        
        hasparent

        adminmodel
        hasadmin
        
        modelname
        
        % Own variables
        namespace
        
        names
        vardims
        
        pnames
        
        aliases

        propfunctions

    end
    
    methods
        
        function model = ComponentModel(modelname, varargin)
            
            model = model@PhysicalModel([]);
            model = merge_options(model, varargin{:});
            model.modelname = modelname;
            % By default, the model has no parent so that the namespace is empty
            model.namespace = {};
            model.hasparent = false;
            % no adminmodel before setup
            model.hasadmin = false;
            % no variables listed by default
            model.names   = {};
            model.pnames  = {};
            model.aliases = {};
            model.vardims = containers.Map();
        end

        function vardim = getvardim(model, name)
            if isKey(model.vardims, name)
                vardim = model.vardims(name);
            else
                vardim = 1;
            end
        end
        
        function state = initiateState(model, state)
            varnames = model.assignCurrentNameSpace(model.names);
            for i = 1 : numel(varnames)
                varname = varnames{i};
                fdname  = varname.getfieldname();
                name    = varname.name;
                if ~isfield(state, fdname)
                    vardim = model.getvardim(name);
                    if vardim == 1
                        state.(fdname) = [];
                    else
                        state.(fdname) = cell(1, vardim);
                    end
                end
            end
        end
        
        function modelname = getModelName(model);
            modelname = model.modelname;
        end
        
        function model = setupAdminModel(model, adminmodel)

            modeldict   = adminmodel.modeldict;
            varnamedict = adminmodel.varnamedict;
            
            % update the dictionary for the models
            [modeldict, model] = setupModelDictAndAdminModel(modeldict, adminmodel, model);
            
            % update dictionary for variables
            [varnames, aliases] = collectVarnames(model);
            adminmodel.varnames = varnames;
            adminmodel.aliases = aliases;
            
            adminmodel = adminmodel.setupVarDict();
            
            % resolve the names in property functions (from relative names to global)
            propfunctions = model.setupPropFunctions();
            propfunctions = unAliasPropFunction(propfunctions, model);
            newpropfunctions = {};
            for ind = 1 : numel(propfunctions)
                propfunction = propfunctions{ind};
                propfunction.id = ind;
                propfunction_s = propfunction.resolveIndex();
                newpropfunctions = horzcat(newpropfunctions, propfunction_s);
            end
            propfunctions = newpropfunctions;
            propfunctions = removeDuplicateProperties(propfunctions);
            
            adminmodel.modeldict     = modeldict;
            adminmodel.varnamedict   = varnamedict;
            adminmodel.propfunctions = propfunctions;

            adminmodel = adminmodel.setupPropfunctionsDict();
        end
            
        function isroot = isRoot(model)
            isroot = ~(model.hasparent);
        end
        
        function name = getModelFullName(model)
            if model.hasparent
                name = join(model.namespace, '_');
                name = name{1};
            else
                name = 'root';
            end
        end
        
        function submodel = getAssocModel(model, name)
            if model.hasadmin
                modeldict = model.adminmodel.modeldict;
                modelfullname = resolveModelName(model, name);
                modelfullname = join(modelfullname, '_');
                modelfullname = modelfullname{1};
                submodel = modeldict(modelfullname);
            else
                error('do not handle case without adminmodel here');
            end
        end
        
        function varnames = getModelPrimaryVarNames(model)
        % List the primary variables. For ComponentModel the variable names only consist of those declared in the model (no child)
            varnames = model.assignCurrentNameSpace(model.pnames);
        end
        

        function varnames = getModelVarNames(model)
            warning('function not updated')
        % List the variable names after resolving the names. 
            names = model.names;
            n = numel(names);
            varnames = {};
            for ind = 1 : n
                name = names{ind};
                varname = VarName({'.'}, name, model.getvardim(name));
                varname = varname.resolveNaming(model);
                varnames{end + 1} = varname;
            end
        end

        function aliases = getModelAliases(model)
            warning('function not updated')
            % List the aliases after resolving the names. 
            if ~hasadmin
            n = numel(model.aliases);
            aliases = {};
            for ind = 1 : n
                name = model.aliases{ind}{1};
                % we use assumption that alias cannot be a cell (aliasvarname.dim = 1, see VarName)
                aliasvarname = VarName({'.'}, name, 1);
                aliasvarname = aliasvarname.resolveNaming(model);
                varname = resolveAlias(model, name, ':');
                aliases{end + 1} = {aliasvarname, varname};
            end
            else
                error('not updated');
            end
        end
        
        function propfunctions = setupPropFunctions(model)
        % resolve naming in propfunctions (from relative to global)
            n = numel(model.propfunctions);
            propfunctions = {};
            for ind = 1 : n
                propfunction = model.propfunctions{ind};
                propfunction = propfunction.resolveNaming(model);
                propfunctions{ind} = propfunction;
            end
        end
        
        function names = getVarFieldNames(model)
        % Returns full names (i.e. including namespaces)
            varnames = model.getModelVarNames();
            names = {};
            for i = 1 : numel(varnames)
                names{end + 1} = varnames{i}.getfieldname;
            end
        end
        
        function names = getPrimaryVarFieldNames(model)
        % Returns full names (i.e. including namespaces)
            varnames = model.getModelPrimaryVarNames();
            names = {};
            for i = 1 : numel(varnames)
                names{end + 1} = varnames{i}.getfieldname;
            end
        end
        
        function model = setAlias(model, alias)
        % If there exist a name in aliases that correspond to alias, overwrite this entry with
        % alias. Otherwise, add alias to aliases.
            isdefined = false;
            name = alias{1};
            aliases = model.aliases;
            for ind = 1 : numel(aliases)
                if strcmp(name, aliases{ind}{1})
                    isdefined = true;
                    break
                end
            end
            
            if isdefined
                aliases{ind} = alias;
            else
                aliases{end + 1} = alias;
            end
            
            model.aliases = aliases;
            
        end
        

        function model = addPropFunction(model, name, fn, inputnames, modelnamespace)
        % We add the propfunction in the list
        %  - Relative naming is assumed as input
        %  - We do not check for duplicate
            
            if iscell(name) && numel(name) > 1
                % The model is a path. Since this function is typically called before the adminmodel is set up we implement an explicit version
                submodelname = name{1};
                name = name(2 : end);
                submodel = model.getAssocModel({submodelname});
                submodel = submodel.addPropFunction(name, fn, inputnames, modelnamespace);
                model = model.setSubModel(submodelname, submodel);
            elseif iscell(name) && numel(name) == 1
                model = model.addPropFunction(name{1}, fn, inputnames, modelnamespace);
            else
                propfunction = PropFunction(name, fn, inputnames, modelnamespace);
                model.propfunctions{end + 1} = propfunction;
            end
            
        end
        
        function [isalias, varname] = aliasLookup(model, name, index)
        % check if name is an alias, return varname (with index and dim field updated)
            aliases = model.aliases;
            isalias = false;
            varname = [];
            
            if isempty(aliases)
                return
            end
            
            if nargin < 3
                index = ':';
            end
            
            for ind = 1 : numel(aliases)
                alias = aliases{ind};
                if strcmp(name, alias{1})
                    varname = alias{2};
                    if ischar(varname)
                        varname = VarName({'.'}, varname);
                    end
                    if isnumeric(index)
                        varname.index = varname.index(index);
                    end
                    isalias = true;
                    return
                end
            end
        end
        
        function [val, state] = getUpdatedProp(model, state, name)
            state = model.updateProp(state, name);
            val = model.getProp(state, name);
        end
        
        function substate = extractSliceProp(model, state, substate, name, ind)
            val = model.getProp(state, name);
            if iscell(val)
                for icell = 1 : numel(val)
                    if ~isempty(val{icell})
                        val{icell} = val{icell}(ind);
                    end
                end
            elseif ~isempty(val)
                val = val(ind);
            end
            substate = model.setProp(substate, name, val);
        end
        
        function state = setSliceProp(model, state, name, subval, ind, genelt)
            error('fix cell case')
            val = model.getProp(state, name);
            if isempty(val)
                val = genelt();
            end
            val(ind) = subval;
            state = model.setProp(state, name, val);
        end        
        
        
        function val = getSliceProp(model, state, name,  ind)
            val = model.getProp(state, name);
            if iscell(val)
                for icell = 1 : numel(val)
                    if ~isempty(val{icell})
                        val{icell} = val{icell}(ind);
                    end
                end
            elseif ~isempty(val)
                val = val(ind);
            end
        end        
        
        function state = syncSliceProp(model, state, substate, name, ind, genelt)
            
            val = model.getProp(state, name);
            subval = model.getProp(substate, name);
            
            if iscell(subval)
                for icell = 1 : numel(subval)
                    if ~isempty(subval{icell})
                        if isempty(val{icell})
                            val{icell} = genelt();
                        end
                        val{icell}(ind) = subval{icell};
                    end
                end
            elseif ~isempty(subval)
                if isempty(val)
                    val = genelt;
                end 
                val = subval(ind);
            else
                warning('no assignement has been done');
            end
            state = model.setProp(state, name, val);
            
        end        
        
        function state = updateProp(model, state, name)

            val = model.getProp(state, name);
            
            % check for vector variables
            isupdated = false;
            
            if ~isempty(val) 
                isupdated = true;
            end
            
            % Check for cell variables
            if iscell(val)
                isupdated = true;
                for ind = 1 : numel(val)
                    if isempty(val{ind})
                        isupdated = false;
                        break
                    end
                end
            end
            
            if isupdated
                return
            end
            
            if isa(name, 'VarName')
                varname = name;
                varname = varname.resolveNaming(model);
            elseif iscell(name)
                % syntaxic sugar (do not need to setup VarName)
                % here we should be consistent with ComponentModel.getModelFullName (we should have implemented this in a separate function)
                modelname = {model.namespace{:}, name{1 : end - 1}};
                name = name{end};
                if iscell(modelname) & isempty(modelname)
                    dictmodelname = 'root';
                elseif iscell(modelname) 
                    dictmodelname = join(modelname, '_');
                    dictmodelname = dictmodelname{1};
                end
                submodel = model.adminmodel.modeldict(dictmodelname);
                varname = VarName(modelname, name, submodel.getvardim(name));
                varname.isNamingRelative = false;
            else
                varname = VarName(model.namespace, name, model.getvardim(name));
                varname.isNamingRelative = false;
            end

            varname = resolveAlias(varname, model.adminmodel.getRoot());
            varnames = varname.resolveIndex();
            
            propfunctions = {};
            priorities = [];
            indp = 1;
            for ind = 1 : numel(varnames)
                name = varnames{ind}.getIndexedFieldname;
                if model.adminmodel.propfuncdict.isKey(name)
                    propfunctions{indp} = model.adminmodel.propfuncdict(name);
                    priorities(indp) = propfunctions{indp}.priority;
                    indp = indp + 1;
                end
            end
            
            [~, ind] = sort(priorities, 'descend');
            
            propfunctions = propfunctions(ind);
            ids = cellfun(@(pf) (pf.id), propfunctions);
            [~, ind] = unique(ids);
            propfunctions = propfunctions(ind);            
            
            for ind = 1 : numel(propfunctions)
                fn = propfunctions{ind}.fn;
                fnmodelname = propfunctions{ind}.modelnamespace;
                % here we should be consistent with ComponentModel.getModelFullName (we should have implemented this in a separate function)
                if iscell(fnmodelname) & isempty(fnmodelname)
                    fnmodelname = 'root';
                elseif iscell(fnmodelname) 
                    fnmodelname = join(fnmodelname, '_');
                    fnmodelname = fnmodelname{1};
                end
                
                fnmodel = model.adminmodel.modeldict(fnmodelname);
                state = fn(fnmodel, state);
                
            end

        end

        function stateAD = initStateAD(model, state)
            
            pvarnames = model.getModelPrimaryVarNames();
            stateAD = model.initiateState([]);
            for ind = 1 : numel(pvarnames)
                vars{ind} = model.getProp(state, pvarnames{ind});
            end
            [vars{:}] = model.AutoDiffBackend.initVariablesAD(vars{:});
            
            for ind = 1 : numel(pvarnames)
                stateAD = model.setProp(stateAD, pvarnames{ind}, vars{ind});
            end
            
            if isfield(state, 'time')
                stateAD.time = state.time;
            end
            
        end
        
        function dv = getIncrement(model, dx, problem, name)
            pnames = problem.primaryVariables;
            isfound = false;
            for ind = 1 : numel(pnames)
                if name == pnames{ind}
                    isfound = true;
                    break
                end
            end
            assert(isfound, 'primary variable not found');
            dv = dx{ind};
        end
        
        function [fn, index] = getVariableField(model, name)
            
            if isa(name, 'VarName')
                varname = name;
                varname = varname.resolveNaming(model);
            elseif iscell(name)
                % syntaxic sugar (do not need to setup VarName)
                varname = VarName({model.namespace{:}, name{1 : end - 1}}, name{end}, model.getvardim(name));
                varname.isNamingRelative = false;
            else
                varname = VarName(model.namespace, name, model.getvardim(name));
                varname.isNamingRelative = false;
            end

            varname = model.adminmodel.varnamedict(varname.getfieldname);
            
            fn = varname.getfieldname;
            index = varname.index;
            
        end
        
        function rstate = reduceState(model, state, varargin)
        % Reduce state and remove AD
            
            rstate = model.initiateState([]);
            pnames = model.getModelPrimaryVarNames();
            for ind = 1 : numel(pnames)
                pname = pnames{ind};
                val = model.getProp(state, pname);
                rstate = model.setProp(rstate, pname, value(val));
            end

            if isfield(state, 'time')
                rstate.time = state.time;
            end
            
        end
        function [p, state] = getProp(model, state, name)
        % Get a single property from the nonlinear state
        %
        % SYNOPSIS:
        %   p = model.getProp(state, 'pressure');
        %
        % PARAMETERS:
        %   model - Class instance.
        %   state - `struct` holding the state of the nonlinear problem.
        %   name  - A property name supported by the model's
        %           `getVariableField` mapping.
        %
        % RETURNS:
        %   p     - Property taken from the state.
        %   state - The state (modified if property evaluation was done)
        %
        % SEE ALSO:
        %   `getProps`
            [fn, index] = model.getVariableField(name);
            if isempty(fn)
                % Not known - check property functions
                containers = model.getStateFunctionGroupings();
                nc = numel(containers);
                if nc > 0 && ~containers{1}.isStateInitialized(state)
                    state = model.initStateFunctionContainers(state);
                end
                for i = 1:nc
                    c = containers{i};
                    nms = c.getNamesOfStateFunctions();
                    sub = strcmpi(nms, name);
                    if any(sub)
                        [p, state] = c.get(model, state, nms{sub});
                        return
                    end
                end
                error('PhysicalModel:UnknownVariable', ...
                      'I did not find %s in any state function grouping or via getVariableField for model of type %s', name, class(model));
            else
                if iscell(state.(fn))
                    if ischar(index)
                        p = state.(fn);
                    elseif numel(index) == 1
                        p = state.(fn){index};
                    else
                        p = state.(fn);
                        p = p(index);
                    end
                else
                    p = state.(fn)(:, index);
                end
            end
        end

        function state = setProp(model, state, name, value)
        % Set named state property to given value
        %
        % SYNOPSIS:
        %   state = model.setProp(state, 'PropertyName', value)
        %
        % PARAMETERS:
        %   model - Class instance.
        %   state - `struct` holding the state of the nonlinear problem.
        %   name  - Name of the property to updated. See `getVariableField`
        %   value - The updated value that will be set.
        %
        % RETURNS:
        %   state - Updated state `struct`.
        %
        % EXAMPLE:
        %   % This will set state.pressure to 5 if the model knows of a
        %   % state field named pressure. If it is not known, it will
        %   % result in an error.
        %   state = struct('pressure', 0);
        %   state = model.setProp(state, 'pressure', 5);

            [fn, index] = model.getVariableField(name);
            present = isfield(state, fn);
            unit = present && (size(state.(fn), 2) == 1);
            if ischar(index) && strcmp(index, ':') || unit
                state.(fn) = value;
            else
                if isa(value, 'ADI') && ~iscell(state.(fn))
                    % Expand to cell array since AD does not support matrices
                    sz = size(state.(fn), 2);
                    state.(fn) = arrayfun(@(x) state.(fn)(:, x), 1:sz, ...
                                          'UniformOutput', false);
                end
                if iscell(state.(fn))
                    if ischar(index)
                        state.(fn) = value;
                    elseif numel(index) == 1 
                        state.(fn){index} = value;
                    else
                        for ind = 1 : numel(index)
                            state.(fn){index(ind)} = value{ind};
                        end
                    end
                else
                    state.(fn)(:, index) = value;
                end
            end
        end


        function varnames = assignCurrentNameSpace(model, names)
        % utility function which returns a cell consisting of the current model namespace with the same size as names.
            n = numel(names);
            varnames = {};
            for ind = 1 : n
                varname = VarName({'.'}, names{ind});
                varname = varname.resolveNaming(model);
                varnames{end + 1} = varname;
            end
        end
        
        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            report = [];
        end
        
    end

end
