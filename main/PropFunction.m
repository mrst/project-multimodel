classdef PropFunction
    
    properties
        
        varname
        
        inputvarnames
        modelnamespace 
        
        fn % function handler
        
        isNamingRelative

        id % id number (it is used to avoid multiple evaluation of the same property function)
        priority % used to order the evaluation of the property functions in case of multiple candidates.
        
    end
    
    methods
        
        function propfunction = PropFunction(varname, fn, inputvarnames, modelnamespace, isNamingRelative)
            
            if nargin < 5
                isNamingRelative = true;
                propfunction.isNamingRelative = true;
                if isa(varname, 'VarName')
                    propfunction.varname = varname;
                elseif ischar(varname)
                    propfunction.varname = VarName({'.'}, varname);
                else
                    error('format for varname is not recognized.');
                end
            else
                propfunction.varname = varname;
            end
            
            propfunction.isNamingRelative = isNamingRelative;
            
            for ind = 1 : numel(inputvarnames)
                inputvarname = inputvarnames{ind};
                if ischar(inputvarname)
                    assert(isNamingRelative, 'wrong format for inputvarname');
                    inputvarname = VarName({'.'}, inputvarname);
                elseif iscell(inputvarname)
                    assert(isNamingRelative, 'wrong format for inputvarname');
                    assert(numel(inputvarname) > 1, 'wrong format for inputvarname');
                    inputvarname = VarName({inputvarname{1 : (end - 1)}}, inputvarname{end});
                else
                    assert(isa(inputvarname, 'VarName'), 'wrong format for inputvarname')
                end
                inputvarnames{ind} = inputvarname;
            end
            
            propfunction.fn = fn;
            propfunction.inputvarnames = inputvarnames;
            propfunction.modelnamespace = modelnamespace;
            
        end
        
        function propfunction = resolveNaming(propfunction, model)
        % Set relative name space to global namespace
            
            isNamingRelative = propfunction.isNamingRelative;
            
            if isNamingRelative
                
                propfunction.varname = propfunction.varname.resolveNaming(model);
                
                inputvarnames = propfunction.inputvarnames;
                for ind = 1 : numel(inputvarnames)
                    inputvarname = inputvarnames{ind};
                    inputvarname = inputvarname.resolveNaming(model);
                    inputvarnames{ind} = inputvarname;
                end
                propfunction.inputvarnames = inputvarnames;
                
                model = model.getAssocModel(propfunction.modelnamespace);
                propfunction.modelnamespace = model.namespace;
                
                propfunction.isNamingRelative = false;
                
            end
            
        end
        
        function propfunctions = resolveIndex(propfunction)
        % resolve the index for varname (when propfunction.varname is a cell, duplicate the propfunction for each cell entry)
            varname = propfunction.varname;
            varnames = varname.resolveIndex();
            propfunctions = {};
            for ind = 1 : numel(varnames)
                propfunctions{ind} = propfunction;
                propfunctions{ind}.varname = varnames{ind};
            end
            
        end

    end
    
    
end
