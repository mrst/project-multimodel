classdef AdminModel < handle
    
    properties
        
        varnames
        aliases

        propfunctions
        
        % dictionaries for rapid access to properties
        propfuncdict
        modeldict
        varnamedict
        
    end
    
    methods
        
        function model = AdminModel(name, varargin)

            model.modeldict     = containers.Map;
            model.propfuncdict  = containers.Map;
            model.varnamedict   = containers.Map;
            
        end

        function rootmodel = getRoot(model)
        % The root model is identified with the key 'root' in modeldict
            rootmodel = model.modeldict('root');
        end

        function model = setupPropfunctionsDict(model, priorities)
            
            propfunctions = model.propfunctions;
            
            for ind = 1 : numel(propfunctions)
                propfunction = propfunctions{ind};
                varname = propfunction.varname;
                name = varname.getIndexedFieldname();
                model.propfuncdict(name) = propfunction;
            end
            
        end
        

        function model = setupVarDict(model)
            
            varnames = model.varnames;
            aliases = model.aliases;
            varnamedict = model.varnamedict;
            
            for ind = 1 : numel(varnames)
                varname = varnames{ind};
                name = varname.getfieldname();
                varnamedict(name) = varname;
            end
            
            for ind = 1 : numel(aliases)
                alias = aliases{ind};
                aliasname = alias{1}.getfieldname();
                varnamedict(aliasname) = alias{2};
            end
            
        end
        
        function c = printModelDict(model)

            k = model.modeldict.keys;
            v = model.modeldict.values;
            
            c = vertcat(k, v)';

        end
        
        function c = printVarnames(model)
            
            varnames = model.varnames();
            
            for ind = 1 : numel(varnames)
                fprintf('"%s"\n', varnames{ind}.getfieldname);
            end
            
        end
        
        function c = printAliases(model)
            
            aliases = model.aliases;
            
            for ind = 1 : numel(aliases)
                alias = aliases{ind};
                fprintf('"%s"\n', alias{1}.getfieldname);
            end
            
        end
        
        function c = printPropfunctions(model)
            
            propfunctions = model.propfunctions;
            
            inputvarstrs = {};
            funcstrs = {};
            varstrs = {};
            
            for ind = 1 : numel(propfunctions)
                propfunction = propfunctions{ind};
                inputvarstr = sprintf('(');
                inputvarnames = propfunction.inputvarnames;
                n = numel(inputvarnames);
                for ind = 1 : n
                    inputvarstr = [inputvarstr, printvar(inputvarnames{ind})];
                    if ind < n
                        inputvarstr = [inputvarstr, ', '];
                    else
                        inputvarstr = [inputvarstr, ')'];
                    end
                end

                inputvarstrs{end + 1} = inputvarstr;
                funcstrs{end + 1}     = func2str(propfunction.fn);
                varstrs{end + 1}      = printvar(propfunction.varname);
                
            end
            
            fmt = sprintf('%%-%ds -- %%-%ds --> %%-%ds\n', maxlength(inputvarstrs), ...
                                                    maxlength(funcstrs), ...
                                                    maxlength(varstrs));
            
            for ind = 1 : numel(varstrs)
                fprintf(fmt, inputvarstrs{ind}, funcstrs{ind}, varstrs{ind});
            end
            
        end
        
        
    end
end

function l = maxlength(strs)
    l = cellfun(@(str) length(str), strs);
    l = max(l);
end

    
function str = printvar(varname)
    index = varname.index;
    str = sprintf(varname.getfieldname);
    if ~(ischar(index) & strcmp(index, ':'))
        indstr = sprintf('%d', varname.index(1));
        for ind = 2 : numel(varname.index)
            indstr = sprintf('%s,%d', indstr, varname.index(ind));
        end
        str = sprintf('%s[%s]', str, indstr);
    end
end
