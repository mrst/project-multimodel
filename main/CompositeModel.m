classdef CompositeModel < ComponentModel

    properties
        
        SubModels;
        isCompositeModel;
        
    end

    methods
        
        function model = CompositeModel(name, varargin)
        % The constructor function should be complemented so that the properties 
        % SubModels are defined
        % the function initiateCompositeModel, which sets up the namespaces, MUST be called in the constructor and it
        % must be called AT THE END.
            model = model@ComponentModel(name, varargin{:});
            model.hasparent = false;
        end
        
        function submodel = getAssocModel(model, name)
            if ~model.hasadmin
                % this may be used in constructor before adminmodel is properly set up.
                model = initiateCompositeModel(model);
                submodel = model.getAssocModel(name);
            else
                submodel = getAssocModel@ComponentModel(model, name);
            end
        end

        function model = setSubModel(model, name, submodel)
            submodelnames = cellfun(@(m) m.getModelName(), model.SubModels, 'uniformoutput', false);
            ind = strcmp(name, submodelnames);    
            model.SubModels{ind} = submodel;
        end

        function model = initiateCompositeModel(model)
            
            model.isCompositeModel = true;
            
            if ~model.hasparent
                model.namespace = {};
            end
            
            % Setup the namespaces and names for all the submodels
            for ind = 1 : numel(model.SubModels)
                submodel = model.SubModels{ind};
                submodel.hasparent = true; 
                
                submodel.namespace = horzcat(model.namespace, {submodel.getModelName()});
                
                if isa(submodel, 'CompositeModel')
                    submodel = submodel.initiateCompositeModel();
                end
                
                model.SubModels{ind} = submodel;
            end
            
            if ~model.hasparent
                adminmodel = AdminModel();
                model = model.setupAdminModel(adminmodel);
            end
            
        end
        

        function state = initiateState(model, state)
            state = initiateState@ComponentModel(model, state);
            for i = 1 : numel(model.SubModels)
                submodel = model.SubModels{i};
                state = submodel.initiateState(state);
            end
        end
        
        function varnames = getModelPrimaryVarNames(model)
        
            varnames = model.assignCurrentNameSpace(model.pnames);
            for i = 1 : numel(model.SubModels)
                submodel = model.SubModels{i};
                varnames1 = submodel.getModelPrimaryVarNames();
                varnames = horzcat(varnames, varnames1);
            end
            
        end
        
        function varnames = getModelVarNames(model)
            warning('not updated');
            % default for compositemodel : fetch all the defined names in the submodels. The returned names are resolved
            if ~hasadmin
                varnames = getModelVarNames@ComponentModel(model);
                for ind = 1 : numel(model.SubModels)
                    submodel = model.SubModels{ind};
                    varnames1 = submodel.getModelVarNames();
                    varnames = horzcat(varnames, varnames1);
                end
            else
                error
            end
            
        end
        
        function varnames = getModelAliases(model)
            warning('not updated')
            % default for compositemodel : fetch all the defined aliases in the submodels
        
            if ~hasAdmin
                varnames = getModelAliases@ComponentModel(model);
                for ind = 1 : numel(model.SubModels)
                    submodel = model.SubModels{ind};
                    varnames1 = submodel.getModelAliases();
                    varnames = horzcat(varnames, varnames1);
                end
            else
                varnames = getModelAliases@ComponentModel(model);
            end
            
        end

        function propfunctions = setupPropFunctions(model)
        % resolve naming in propfunctions (from relative to global)
            
            propfunctions = setupPropFunctions@ComponentModel(model);
            for ind = 1 : numel(model.SubModels)
                submodel = model.SubModels{ind};
                propfunctions1 = submodel.setupPropFunctions();
                propfunctions = horzcat(propfunctions, propfunctions1);
            end
            
        end

    end
    
end
