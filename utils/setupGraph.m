function [g, edgelabels] = setupGraph(model, varargin)
    
    opt = struct('excludeVarnames', false, ...
                 'resolveIndex', true);
    opt = merge_options(opt, varargin{:});
    
    g = digraph();
    
    ss = {}; % source nodes
    ts = {}; % target nodes
    fs = {}; % edge function name
    ms = {}; % edge model name    
    
    if ~opt.excludeVarnames
        varnames = model.adminmodel.varnames;

        for ind = 1 : numel(varnames)
            varname = varnames{ind};
            if opt.resolveIndex
                varname_s = varname.resolveIndex();
                for ind = 1 : numel(varname_s)
                    fullname = varname_s{ind}.getIndexedFieldname();
                    g = addnode(g, fullname);
                end
            else
                fullname = varname.getfieldname;
                g = addnode(g, fullname);
            end
        end
        
    end
    
    propfunctions = model.adminmodel.propfunctions;

    for ind = 1 : numel(propfunctions)

        propfunction = propfunctions{ind};
        varname = propfunction.varname;
        m = propfunction.modelnamespace;
        
        f = func2str(propfunction.fn);
        inputvarnames = propfunction.inputvarnames;
        
        fullinputvarnames = {};
        
        for i = 1 : numel(inputvarnames)
            inputvarname = inputvarnames{i};
            if opt.resolveIndex
                inputvarnames_s =  inputvarname.resolveIndex();
                for j = 1 : numel(inputvarnames_s)
                    fullinputvarname = inputvarnames_s{j}.getIndexedFieldname();
                    fullinputvarnames = horzcat(fullinputvarnames, {fullinputvarname});
                end
            else
                inputvarname = inputvarname.getfieldname;
                fullinputvarnames = horzcat(fullinputvarnames, {inputvarname});
            end
        end

        if opt.resolveIndex
            varnames = varname.resolveIndex();
            for ind = 1 : numel(varnames)
                fullvarnames{ind} = varnames{ind}.getIndexedFieldname();
            end
        else
            fullvarnames = {varname.getfieldname};
        end
        
        nv = numel(fullvarnames);
        ni = numel(fullinputvarnames);
        
        indv = rldecode((1 : nv)', ni*ones(nv, 1))';
        indi = repmat((1 : ni)', 1, nv);
        
        s = fullinputvarnames(indi);
        t = fullvarnames(indv);
        f = repmat({f}, 1, nv*ni);
        m = repmat({m}, 1, nv*ni);
        
        ss = horzcat(ss, s);
        ts = horzcat(ts, t);
        fs = horzcat(fs, f);
        ms = horzcat(ms, m);
        
    end
    
    g = addedge(g, ss, ts);
    
    edgelabels.ss = ss;
    edgelabels.ts = ts;
    edgelabels.fs = fs;
    edgelabels.ms = ms;
    
end

