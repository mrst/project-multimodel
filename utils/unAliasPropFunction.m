function propfunctions = unAliasPropFunction(propfunctions, model)
%
% Removes all the aliases in the propfunctions
%
% In particular, it is called in Component.setupAdminModel
%
    rootmodel = model.adminmodel.getRoot();
    for ind = 1 : numel(propfunctions)
        propfunction = propfunctions{ind};
        % unalias varname
        propfunction.varname = resolveAlias(propfunction.varname, rootmodel);
        % unalias inputvarnames
        inputvarnames = propfunction.inputvarnames;
        for indinput = 1 : numel(inputvarnames)
            inputvarnames{indinput} = resolveAlias(inputvarnames{indinput}, rootmodel);
        end
        propfunction.inputvarnames = inputvarnames;
        propfunctions{ind} = propfunction;        
    end
    
end
