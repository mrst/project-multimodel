function setupOrderedFunctions(model, filename)
    
    if nargin < 2
        useoutputfile = false;
    end

    adminmodel = model.adminmodel;
    propfuncdict = adminmodel.propfuncdict;
    propfuncdict = adminmodel.propfuncdict;
    
    [g, edgelabels] = setupGraph(model);
    
    nn = g.numnodes;
    nodenames = g.Nodes.Variables;
    
    
    A = adjacency(g);
    A = A';

    p = topological_order(A);
    p = p(end : -1  : 1);

    fnnames = {};
    
    for ind = 1 : nn
        pind = p(ind);
        % find incoming edges
        [eid, nin] = inedges(g, pind);

        if ~isempty(eid)
            
            nodename = nodenames{pind};
            propfunction = propfuncdict(nodename);
            
            fn = propfunction.fn;
            fnname = stripFunctionName(fn);
            modelname = propfunction.modelnamespace;
            fnname = strcat(modelname{:}, fnname);
            
            if ~ismember(fnname, fnnames)
                fnnames = {fnnames{:}, fnname}; 
            end
            
        end

    end

    if useoutputfile
        f = fopen(filename, 'w');
    else
        f = 1;
    end
    
    for ind = 1 : numel(fnnames)
        fprintf(f, '%s\n', fnnames{ind});
    end
    
    if useoutputfile
        fclose(f);
    end
    
end


function fnname = stripFunctionName(fn)
    fnname = func2str(fn);
end
