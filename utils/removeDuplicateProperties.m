function propfunctions = removeDuplicateProperties(propfunctions)
% Remove duplicates in the propfunctions list. 
%
% In case of duplicate, pick up the last entry in the list (which should correspond to the one added last).
%
% Moreover, a priority number is assigned to each propfunction. This is used in ComponentModel.updateProp(state,
% name). Because name can refer to a cell, it can hit several propfunctions as each entry in the cell structure can have
% his own propfunction. To determine the evaluation order, we need the information encoded by the priority numbers.
%
%   
    
    varnames = cellfun(@(propfunction) propfunction.varname, propfunctions, 'uniformoutput', false);
    fullnames = cellfun(@(varname) varname.getIndexedFieldname(), varnames, 'uniformoutput', false);

    [~, ia, ic] = unique(fullnames, 'last');
    
    propfunctions = propfunctions(ia);
    for ind = 1 : numel(propfunctions)
        propfunctions{ind}.priority = ic(ind);
    end    
end
