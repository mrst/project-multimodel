function [modeldict, model] = setupModelDictAndAdminModel(modeldict, adminmodel, model)
%
% Assign adminmodel in each of the submodels of model and setup the model dictionary
%
    if isa(model, 'CompositeModel')
        for ind = 1 : numel(model.SubModels)
            submodel = model.SubModels{ind};
            [modeldict, submodel] = setupModelDictAndAdminModel(modeldict, adminmodel, submodel);
            model.SubModels{ind} = submodel;
        end
    end
    
    model.adminmodel = adminmodel;
    model.hasadmin = true;
    name = model.getModelFullName;
    modeldict(name) = model;
    
end
