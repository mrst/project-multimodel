function globnamespace = resolveModelName(model, relnamespace)
%
% Given a model and a relative submodel path given by relnamespace, construct the global namespace for this submodel.
%
% This function removes all instances such as '..', '.'
%
    modelnamespace = horzcat({'root'}, model.namespace);
    globnamespace = resolveModelNameIter(modelnamespace, relnamespace);
    assert(strcmp(globnamespace{1}, 'root'), 'The first model should be root');
    if numel(globnamespace) > 1
        globnamespace = globnamespace(2 : end);
    end
    
end

function [firstpart, secondpart] = resolveModelNameIter(firstpart, secondpart)
    
    if isempty(secondpart)
        return
    else
        elt = secondpart{1};
        secondpart = secondpart(2 : end);
        if strcmp(elt, '.')
            [firstpart, secondpart] = resolveModelNameIter(firstpart, secondpart);
        elseif strcmp(elt, '..')
            firstpart = firstpart(1 : (end - 1));    
            [firstpart, secondpart] = resolveModelNameIter(firstpart, secondpart);
        else
            firstpart = horzcat(firstpart, {elt});
            [firstpart, secondpart] = resolveModelNameIter(firstpart, secondpart);
        end
    end

end

