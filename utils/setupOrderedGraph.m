function setupOrderedGraph(model, filename)

% only write to output file for the moment
    
    [g, edgelabels] = setupGraph(model);
    propfuncdict = model.adminmodel.propfuncdict;
    modeldict = model.adminmodel.modeldict;
    
    ss = edgelabels.ss; % source nodes
    ts = edgelabels.ts; % target nodes
    fs = edgelabels.fs; % edge function name
    ms = edgelabels.ms; % edge model name
    
    A = adjacency(g);
    A = A';

    nn = g.numnodes;

    p = topological_order(A);
    p = p(end : -1  : 1);

    nodenames = g.Nodes.Variables;

    f = fopen(filename, 'w');

    for ind = 1 : nn
        pind = p(ind);
        % find incoming edges
        [eid, nin] = inedges(g, pind);

        if ~isempty(eid)
            
            nodename = nodenames{pind};
            
            printcomment(f, model, nodename);

            fprintf(f, 'propfunction = propfuncdict(%s);\n', nodename);
            fprintf(f, 'fn = propfunction.fn;\n');
            fprintf(f, 'model = propfunction.model;\n');
            fprintf(f, 'modelname = model.getModelFullName;\n');
            fprintf(f, 'model = modeldict(modelname);\n');
            fprintf(f, 'state = fn(model, state);\n\n\n');
            
        end
    end
    
    fclose(f);

end

function printcomment(f, model, nodename)
    
    adminmodel = model.adminmodel;
    
    propfuncdict = adminmodel.propfuncdict;
    modeldict = adminmodel.modeldict;
    
    propfunction = propfuncdict(nodename);
    
    % setup variable name (which is updated)
    varname = nodename;
    
    % setup function name
    fn = propfunction.fn;
    fnname = stripFunctionName(fn);
    
    % setup model
    modelname = propfunction.modelnamespace;
    submodel = model.getAssocModel(modelname);
    modelname = submodel.getModelFullName;
    
    % setup input names
    inputnames = {};
    inputvarnames = propfunction.inputvarnames;
    for ind = 1 : numel(inputvarnames)
        inputvarname = inputvarnames{ind};
        inputvarname_s = inputvarname.resolveIndex();
        for ind2 = 1 : numel(inputvarname_s)
            inputname = inputvarname_s{ind2};
            inputname = inputname.getIndexedFieldname();
            inputnames{end + 1} = inputname;
        end
    end        
    
    % write to file
    fprintf(f, '%%%%%%%%%%%%%%%%%%%% \n'); 
    fprintf(f, '%%%% Computed variable : %s\n', varname);
    fprintf(f, '%%%% Input variables : \n');    
    for ind = 1 : numel(inputnames)
        fprintf(f, '%%  - %s\n', inputnames{ind});
    end
    fprintf(f, '%%%% Function used : %s\n', fnname);
    fprintf(f, '%%%% Input model : %s\n\n', modelname);
    
end

function fnname = stripFunctionName(fn)
    fnname = func2str(fn);
end
