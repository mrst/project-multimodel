function varname = resolveAlias(aliasvarname, rootmodel)
%
% Resolve the alias (recursively) for an aliasvarname 
%
% The namespace of aliasvarname should be resolved before calling this function. This function can therefore only be
% called when the whole model has been initiated and the rootmodel identified. (we need a well-defined getAssocModel
% method that can navigate in the model and accross the submodels).
    
    assert(~aliasvarname.isNamingRelative, 'cannot fully resolve aliases for relative naming (use VarName.resolveNaming before sending variable)')
    assert(rootmodel.isRoot(), 'the root model should be sent to this function');
    
    aliasname = aliasvarname.name;
    aliasmodel = rootmodel.getAssocModel(aliasvarname.namespace);

    [isalias, varname] = aliasmodel.aliasLookup(aliasname);
    
    if isalias
        varname = varname.resolveNaming(aliasmodel);
        index = varname.index;
        varname = resolveAlias(varname, rootmodel);
        if ~(ischar(index) && strcmp(index, ':'))
            indices = (1 : varname.dim);
            varname.index = indices(index);
        end
    else
        varname = aliasvarname;
    end
    
    
    
end
