function [varnames, aliases] = collectVarnames(model, varnames, aliases)
% Collect recursively in the model and submodels all the variables and aliases
% The varnames and aliases are returned with resolved names.
    if nargin < 2
        varnames = {};
        aliases = {};
    end
        
    names = model.names;
    for ind = 1 : numel(names)
        name = names{ind};
        varname = VarName({'.'}, name);
        varname = varname.resolveNaming(model);
        varnames{end + 1} = varname;
    end
    
    aliasnames = model.aliases;
    rootmodel = model.adminmodel.getRoot();
    
    for ind = 1 : numel(aliasnames)
        name = aliasnames{ind}{1};
        aliasvarname = VarName({'.'}, name);
        aliasvarname = aliasvarname.resolveNaming(model);
        varname = resolveAlias(aliasvarname, rootmodel);
        aliases{end + 1} = {aliasvarname, varname};
    end
    
    if isa(model, 'CompositeModel')
        for ind = 1 : numel(model.SubModels)
            [varnames, aliases] = collectVarnames(model.SubModels{ind}, varnames, aliases);
        end
    end
end
