clear all
close all

mrstModule add compositional multimodel ad-core ad-blackoil ad-props

G = cartGrid([100 1], [1 1]);
G = computeGeometry(G);

bc = pside([], G, 'LEFT',  1*barsa, 'sat', [1, 0]);
bc = pside(bc, G, 'RIGHT', 0*barsa, 'sat', [0, 1]);

rock = makeRock(G, 100*milli*darcy, 0.3);

fluid = initSimpleADIFluid('phases','WO',              ... % Fluid phase: water
                           'mu',  [1, 50]*centi*poise, ... % Viscosity
                           'rho', [1000, 600],         ... % Surface density [kg/m^3]
                           'n', [2, 2],                ... % Surface density [kg/m^3]
                           'c', [1e-4, 1e-4]/barsa,    ... % Fluid compressibility
                           'cR', 1e-5/barsa            ... % Rock compressibility
                           );

model = TransportModel(G, rock, fluid, bc);

testadminmodel = false;
if testadminmodel
    adminmodel = AdminModel();
    model = model.setupAdminModel(adminmodel);
end

nc = G.cells.num;

state = model.initiateState([]);

pressure = 0*barsa*ones(nc, 1);
s = cell(1, 2);
s{1} = 0*ones(nc, 1);

state = model.setProp(state, {'core', 'pressure'}, pressure);
state = model.setProp(state, {'core', 's'}, s);

state = model.updateProp(state, {'core', 'rho'});
state = model.updateProp(state, {'core', 's'});

dt = 1e3*ones(100, 1);
schedule = simpleSchedule(dt, 'bc', bc);

states = runSimulation(state, model, schedule);

sW = model.getProp(states{end}, {'core', 'sW'});
plot(sW);