function state = computeDensities(model, state)
    
    fluid = model.fluid;
    
    [p, state] = model.getUpdatedProp(state, 'pressure');
    
    rhoS{1} = fluid.rhoWS;
    rhoS{2} = fluid.rhoOS;
    
    b{1} = fluid.bW(p);
    b{2} = fluid.bO(p);
    
    rho = cell(1, 2);
    for ind = 1 : 2
        rho{ind} = b{ind}.*rhoS{ind};
    end
    
    state = model.setProp(state, 'rho', rho);
    
end
