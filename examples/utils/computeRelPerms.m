function state = computeRelPerms(model, state)
    
    fluid = model.fluid;

    [sW, state] = model.getUpdatedProp(state, 'sW');
    [sO, state] = model.getUpdatedProp(state, 'sO');

    krW = fluid.krW(sW);
    krO = fluid.krO(sO);
    
    state = model.setProp(state, 'krW', krW);
    state = model.setProp(state, 'krO', krO);
    
end
