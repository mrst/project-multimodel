function state = computeMobilities(model, state)
    
    [kr, state] = model.getUpdatedProp(state, 'kr');
    [mu, state] = model.getUpdatedProp(state, 'mu');
            
    for ind = 1 : numel(kr)
        mob{ind} = kr{ind}./mu{ind};
    end
    
    state = model.setProp(state, 'mob', mob);
    
end