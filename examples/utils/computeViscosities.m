function state = computeViscosities(model, state)
    
    fluid = model.fluid;
    
    [p, state] = model.getUpdatedProp(state, 'pressure');

    muW = fluid.muW(p);
    muO = fluid.muO(p);
    
    state = model.setProp(state, 'muW', muW);
    state = model.setProp(state, 'muO', muO);
    
end
