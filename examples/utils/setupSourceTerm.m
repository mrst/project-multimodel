function state = setupSourceTerm(model, state, bcdata)
    
    nph = 2;
    
    cellToBCMap = bcdata.cellToBCMap;
    BCTocellMap = bcdata.BCTocellMap;
    T           = bcdata.T;
    p_bc        = bcdata.pressure;
    sat_bc      = bcdata.s;
    
    bcmodel   = model.getAssocModel({'bcpress'});
    coremodel = model.getAssocModel({'core'});
    
    state = bcmodel.setProp(state, 'pressure', p_bc);
    state = bcmodel.setProp(state, 's', sat_bc);
    
    [rho_bc, state] = bcmodel.getUpdatedProp(state, 'rho');
    [mob_bc, state] = bcmodel.getUpdatedProp(state, 'mob');
    
    [p_res, state] = coremodel.getUpdatedProp(state, 'pressure');
    [sat_res, state] = coremodel.getUpdatedProp(state, 's');
    [rho_res, state] = coremodel.getUpdatedProp(state, 'rho');
    [mob_res, state] = coremodel.getUpdatedProp(state, 'mob');
    
    p_res = cellToBCMap*p_res;
    for i = 1 : nph
        sat_res{i} = cellToBCMap*sat_res{i};
        rho_res{i} = cellToBCMap*rho_res{i};
        mob_res{i} = cellToBCMap*mob_res{i};
    end

    rhoAvgF = cell(nph, 1);
    for i = 1 : nph
        rhoAvgF{i} = (rho_res{i} + rho_bc{i})./2;
    end

    nbc = numelValue(p_bc);
    zeroAD = model.AutoDiffBackend.convertToAD(zeros(nbc, 1), p_res);

    % Treat pressure BC
    dP = (p_bc - p_res);
    
    % Determine if pressure bc are injecting or producing
    injDir = (dP > 0);
    
    for i = 1 : nph
    
        u{i} = zeroAD;

        if any(~injDir)
            % pressure drives flow outwards
            subs = ~injDir;
            q = rhoAvgF{i}(subs).*mob_res{i}(subs).*T(subs).*dP(subs);
            u{i}(subs) = q;
        end

        if any(injDir)
            % In this case, pressure drives flow inwards, we get the injection rate determined by the sat field
            subs = injDir;
            q = rhoAvgF{i}(subs).*mob_bc{i}(subs).*T(subs).*dP(subs);
            u{i}(subs) = q;
        end
    
        srcterms{i} = BCTocellMap*u{i};
    end
    
    state = model.setProp(state, 'srcterms', srcterms);
    
end
