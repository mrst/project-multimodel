classdef CoreModel < ComponentModel
% Core model 2 phase
    
    properties
        rock
        fluid
        nP = 2; % two phases
        nC = 2; % two components
    end
   
    methods
        
        function model = CoreModel(G, rock, fluid)
            
            model = model@ComponentModel('core');
            
            model.G = G;
            model.rock = rock;
            model.fluid = fluid;
            
            model.operators = setupOperatorsTPFA(G, rock);

            %% Declare model variable names
            
            names = {'pressure', ... % Pressure
                     's'       , ... % Phase saturations
                     'rho'     , ... % Phase densities
                     'kr'      , ... % Phase relative permeabilities
                     'mu'      , ... % Phase viscosities
                     'mob'     , ... % Phase mobilities
                     'u'       , ... % Phase fluxes
                    };
            model.names = names;
            
            nP = model.nP;
            vardims = model.vardims;
            vardims('s') = nP;
            vardims('rho') = nP;
            vardims('kr') = nP;
            vardims('mu') = nP;
            vardims('mob') = nP;
            vardims('u') = nP;
            model.vardims = vardims;
            
            %% Declare primary variable names
            
            pnames = {'pressure', 'sW'};
            
            model.pnames = pnames;
            
            %% setup aliases
            
            nc = 2;
            aliases = {{'rhoW', VarName({'.'}  , 'rho', nc, 1)}, ...
                       {'rhoO', VarName({'.'}  , 'rho', nc, 2)}, ...
                       {'muW' ,  VarName({'.'} , 'mu' , nc, 1)}, ...
                       {'muO' ,  VarName({'.'} , 'mu' , nc, 2)}, ...
                       {'sW'  ,   VarName({'.'}, 's'  , nc, 1)}, ...
                       {'sO'  ,   VarName({'.'}, 's'  , nc, 2)}, ...
                       {'krW' ,  VarName({'.'} , 'kr' , nc, 1)}, ...
                       {'krO' ,  VarName({'.'} , 'kr' , nc, 2)}, ...
                       {'uW'  ,   VarName({'.'}, 'u'  , nc, 1)}, ...
                       {'uO'  ,   VarName({'.'}, 'u'  , nc, 2)}, ...
                      };
            model.aliases = aliases;

            %% setup propfunctions
            
            fn = @(model_, state_) computeRelPerms(model_, state_);
            inputnames = {'s'};
            model = model.addPropFunction('kr', fn, inputnames, {'.'}); %
            
            fn = @(model_, state_) computeMobilities(model_, state_);
            inputnames = {'kr', 'mu'};
            model = model.addPropFunction('mob', fn, inputnames, {'.'}); %

            fn = @(model, state) model.updateOilSaturation(state);
            inputnames = {'sW'};
            model = model.addPropFunction('sO', fn, inputnames, {'.'}); %
            
            fn = @(model_, state_) computeViscosities(model_, state_);
            inputnames = {'pressure'};
            model = model.addPropFunction('mu', fn, inputnames, {'.'}); %

            fn = @(model_, state_) computeDensities(model_, state_);
            inputnames = {'pressure'};
            model = model.addPropFunction('rho', fn, inputnames, {'.'}); %
            
            fn = @(model, state) model.computeFluxes(state);
            inputnames = {'pressure', 'mob'};
            model = model.addPropFunction('u', fn, inputnames, {'.'}); %

        end
        
        
        function state = updateOilSaturation(model, state)
            
            [sW, state] = model.getUpdatedProp(state, 'sW');
            sO = 1 - sW;
            state = model.setProp(state, 'sO', sO);
            
        end
       
        function state = updateSaturations(model, state)
            
            [sW, state] = model.getUpdatedProp(state, 'sW');
            [sO, state] = model.getUpdatedProp(state, 'sO');

            s{1} = sW;
            s{2} = sO;
            
            state = model.setProp(state, 's', s);
        end
        

        function state = computeFluxes(model, state)
            
            op = model.operators;
            T = op.T;
            up = op.faceUpstr;
           
            [p, state]  = model.getUpdatedProp(state, 'pressure');
            [mob, state] = model.getUpdatedProp(state, 'mob');
            
            kgrad = op.T.*op.Grad(p);
            
            u = cell(1, 2);
            for ind = 1 : 2
                fmob = up(kgrad < 0, mob{ind});
                u{ind} = -fmob.*kgrad;
            end
            
            state = model.setProp(state, 'u', u);
            
        end

        function state = computeSrcTerms(model, state, bcdata)

            BCTocellMap = bcdata.BCTocellMap;
            
            bcmodel = model.getAssocModel({'bcpress'});
            [ubc, state] = bcmodel.getUpdatedProp(state, 'u');
            
            for i = 1 : 2
               srcterms{i} = BCTocellMap*ubc{i};
            end
            
            state = model.setProp(state, 'srcterms', srcterms);
            
        end


    end
    
end

