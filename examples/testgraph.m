clear all
close all

mrstModule add compositional multimodel ad-core ad-blackoil ad-props matlab_bgl

G = cartGrid([100 1], [1 1]);
G = computeGeometry(G);

bc = pside([], G, 'LEFT',  1*barsa, 'sat', [1, 0]);
bc = pside(bc, G, 'RIGHT', 0*barsa, 'sat', [0, 1]);

rock = makeRock(G, 100*milli*darcy, 0.3);

fluid = initSimpleADIFluid('phases','WO',              ... % Fluid phase: water
                           'mu',  [1, 50]*centi*poise, ... % Viscosity
                           'rho', [1000, 600],         ... % Surface density [kg/m^3]
                           'n', [2, 2],                ... % Surface density [kg/m^3]
                           'c', [1e-4, 1e-4]/barsa,    ... % Fluid compressibility
                           'cR', 1e-5/barsa            ... % Rock compressibility
                           );

model = TransportModel(G, rock, fluid, bc);

set(0, 'DefaultAxesFontSize', 16);
set(0, 'defaulttextfontsize', 18);
set(0, 'DefaultFigurePosition', [278 109 1570 822]);

gstyle = {'interpreter', 'none', 'LineWidth', 3, 'ArrowSize', 20, 'NodeFontSize', 14};

[g, edgelabels] = setupGraph(model);

plot(g, gstyle{:});

%%

setupOrderedGraph(model, 'test.m');