classdef SourceTermPressureModel < ComponentModel
% Transport Salt CO2 model.

    
    properties
        fluid
        nP = 2; % two phases
        nC = 2; % two components
    end
   
    methods
        
        function model = SourceTermPressureModel(fluid)
            
            model = model@ComponentModel('bcpress');
            
            model.fluid = fluid;

            %% Declare model variable names
            
            names = {'pressure', ... % Pressure
                     's'       , ... % Phase saturations
                     'rho'     , ... % Phase densities
                     'kr'      , ... % Phase relative permeabilities
                     'mu'      , ... % Phase viscosities
                     'mob'     , ... % Phase mobilities
                    };
            
            model.names = names;
            
            nP = model.nP;
            vardims = model.vardims;
            vardims('s') = nP;
            vardims('rho') = nP;
            vardims('kr') = nP;
            vardims('mu') = nP;
            vardims('mob') = nP;
            model.vardims = vardims;
            
            
            %% setup aliases
            
            nc = 2;
            aliases = {{'rhoW', VarName({'.'}  , 'rho', nc, 1)}, ...
                       {'rhoO', VarName({'.'}  , 'rho', nc, 2)}, ...
                       {'muW' ,  VarName({'.'} , 'mu' , nc, 1)}, ...
                       {'muO' ,  VarName({'.'} , 'mu' , nc, 2)}, ...
                       {'sW'  ,   VarName({'.'}, 's'  , nc, 1)}, ...
                       {'sO'  ,   VarName({'.'}, 's'  , nc, 2)}, ...
                       {'krW' ,  VarName({'.'} , 'kr' , nc, 1)}, ...
                       {'krO' ,  VarName({'.'} , 'kr' , nc, 2)}, ...
                      };
            model.aliases = aliases;

            %% setup propfunctions
            
            fn = @(model, state) computeRelPerms(model, state);
            inputnames = {'s'};
            model = model.addPropFunction('kr', fn, inputnames, {'.'}); %
            
            fn = @(model, state) computeMobilities(model, state);
            inputnames = {'kr', 'mu'};
            model = model.addPropFunction('mob', fn, inputnames, {'.'}); %

            fn = @(model, state) computeViscosities(model, state);
            inputnames = {'pressure'};
            model = model.addPropFunction('mu', fn, inputnames, {'.'}); %

            fn = @(model, state) computeDensities(model, state);
            inputnames = {'pressure'};
            model = model.addPropFunction('rho', fn, inputnames, {'.'}); %

            % fn = @(model, state) model.computeFluxes(state);
            % inputnames = {};
            % model = model.addPropFunction('u', fn, inputnames, {'.'}); %

        end
        
    end
    
end

