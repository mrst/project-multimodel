classdef PrevStateModel < ComponentModel
% Transport Salt CO2 model.

    properties
        nP = 2; % two phases
        nC = 2; % two components
    end
    
    methods
        
        function model = PrevStateModel()
            
            model = model@ComponentModel('prevstate');


            %% declare model variable names
            
            names = {'pressure', ... % Pressure
                     's'       , ... % Phase saturations
                     'rho'     , ... % Phase densities
                    };
            
            model.names = names;
            nP = model.nP;
            vardims('s') = nP;
            vardims('rho') = nP;
            vardims = model.vardims;
            
            
            %% setup aliases
            
            nc = 2;
            aliases = {{'rhoW', VarName({'.'}  , 'rho', nc, 1)}, ...
                       {'rhoO', VarName({'.'}  , 'rho', nc, 2)}, ...
                       {'sW'  ,   VarName({'.'}, 's'  , nc, 1)}, ...
                       {'sO'  ,   VarName({'.'}, 's'  , nc, 2)}, ...
                      };
            model.aliases = aliases;
            
        end
        
    end
    
end

