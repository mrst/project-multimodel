classdef TransportModel < CompositeModel
% Transport 2 phase

    methods
        
        function model = TransportModel(G, rock, fluid, bc)
            
            model = model@CompositeModel('transport');

            names = {'massCons', ...
                     'srcterms', ... % Source terms (positive = source, negative = sink)
                     'dt'};
            model.names = names;
            
            propfunctions = {};
                        
            fn = @(model, state) model.setupMassCons(state);
            inputnames = {'srcterms', 'dt', ...
                          VarName({'core'}, 'rho'), ...
                          VarName({'core'}, 's'), ...
                          VarName({'core'}, 'u'), ...
                          VarName({'prevstate'}, 'rho'), ...
                          VarName({'prevstate'}, 's')};
            model = model.addPropFunction('massCons', fn, inputnames, {'.'});
            
            %% setup prevstate model
            
            model.SubModels = {};
            model.SubModels{end + 1} = PrevStateModel();
            model.SubModels{end + 1} = CoreModel(G, rock, fluid);
            model.SubModels{end + 1} = SourceTermPressureModel(fluid);

            %% setup propfunctions for the coupling
            
            bcmodel = model.getAssocModel({'bcpress'});
            coremodel = model.getAssocModel({'core'});

            bcfaces = bc.face;
            bccells = sum(G.faces.neighbors(bcfaces, :), 2);
            nbc = numel(bcfaces);
            
            cellToBCMap = sparse((1 : nbc)', bccells, 1, nbc, G.cells.num);
            BCTocellMap = cellToBCMap';
            T = coremodel.operators.T_all(bcfaces);
            
            bcdata = struct('cellToBCMap', cellToBCMap, ...
                            'BCTocellMap', BCTocellMap, ...
                            'T'          , T          , ...
                            'pressure'   , bc.value   , ...
                            's'          , bc.sat);

            
            fn = @(model_, state_) setupSourceTerm(model_, state_, bcdata);
            inputnames = {VarName({'bcpress'}, 'rho'), ...
                          VarName({'bcpress'}, 'mob'), ...
                          VarName({'core'}, 'pressure'), ...
                          VarName({'core'}, 's'), ...
                          VarName({'core'}, 'rho'), ...
                          VarName({'core'}, 'mob')};
            model = model.addPropFunction('srcterms', fn, inputnames, {'.'});
                     
            %% initiate composite model
            model = model.initiateCompositeModel();
            
        end


        function [problem, state] = getEquations(model, state0, state, dt, forces, varargin)
            
            opt = struct('Verbose'    , mrstVerbose,...
                         'reverseMode', false      ,...
                         'resOnly'    , false      ,...
                         'iteration'  , -1);
            opt = merge_options(opt, varargin{:});

            stateAD = model.initStateAD(state);

            coremodel = model.getAssocModel({'core'});
            bcmodel   = model.getAssocModel({'bcpress'});
            prevmodel = model.getAssocModel({'prevstate'});
            
            %% setup previous state
            
            stateAD = model.setProp(stateAD, 'dt', dt);
            
            %% setup previous state
            
            s0 = coremodel.getProp(state0, 's');
            rho0 = coremodel.getProp(state0, 'rho');
            
            stateAD = prevmodel.setProp(stateAD, 's', s0);
            stateAD = prevmodel.setProp(stateAD, 'rho', rho0);

            %% assemble equations
            
            [eqs, stateAD] = model.getUpdatedProp(stateAD, 'massCons');
            
            %% setup linearized problem
            
            primaryVars = model.getModelPrimaryVarNames();

            [names, types] = deal(cell(1, 2));
            [types{:}] = deal('cell');
            names{1} = 'water';
            names{2} = 'oil';
            
            problem = model.setupLinearizedProblem(eqs, types, names, primaryVars, state, dt);
            
        end

        function [state, report] = updateState(model, state, problem, dx, forces)
            [state, report] = updateState@CompositeModel(model, state, problem, dx, forces);
            state = model.capProperty(state, {'core', 'sW'}, 0, 1);
        end
        
        
        function forces = getValidDrivingForces(model)
            forces.W = [];
            forces.src = [];
            forces.bc = [];
            forces.stopFunction = @(model, state, state0_inner) (false);
        end
        
        function state = setupMassCons(model, state)
            
            model0 = model.getAssocModel({'prevstate'});
            coremodel = model.getAssocModel({'core'});
            
            op = coremodel.operators;
            aver = op.faceAvg;
            pv = op.pv;
            
            [srcterms, state] = model.getUpdatedProp(state, 'srcterms');
            [dt, state] = model.getUpdatedProp(state, 'dt');

            [rho, state] = coremodel.getUpdatedProp(state, 'rho');
            [s, state] = coremodel.getUpdatedProp(state, 's');
            [u, state] = coremodel.getUpdatedProp(state, 'u');
            
            [rho0, state] = model0.getUpdatedProp(state, 'rho');
            [s0, state] = model0.getUpdatedProp(state, 's');
            
            massCons = cell(1, 2);
            
            for ind = 1 : 2
                acc = 1/dt*pv.*(rho{ind}.*s{ind} - rho0{ind}.*s0{ind});
                massCons{ind} = acc + op.Div(aver(rho{ind}).*u{ind}) - srcterms{ind};
            end            
            
            state = model.setProp(state, 'massCons', massCons);
            
        end
        
        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            [state, report] = updateAfterConvergence@ComponentModel(model, state0, state, dt, drivingForces);
            state = model.updateProp(state, {'core', 'rho'});
            state = model.updateProp(state, {'core', 's'});
        end
        
    end
    
end

